import Vue from 'vue';
import App from './app/app.vue';
import router from './app/app.router';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
