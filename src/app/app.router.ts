import Vue from 'vue';
import VueRouter from 'vue-router';
import MarvelCharactersRouter from './modules/marvel-characters/marvel-characters.router';
import MarvelCharactersCartRouter from './modules/marvel-characters-cart/marvel-characters-cart.router';

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: 'marvel-characters',
    },
    ...MarvelCharactersRouter,
    ...MarvelCharactersCartRouter,
  ],
});

Vue.use(VueRouter);

export default router;
