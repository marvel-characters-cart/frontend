export default [
  {
    path: '/marvel-characters-cart',
    name: 'marvel-characters-cart',
    component: () => import('./marvel-characters-cart.vue'),
    title: 'marvel-characters-cart',
  },
];
