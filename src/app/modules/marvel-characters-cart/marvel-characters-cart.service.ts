import { AxiosResponse } from 'axios';
import localStorageService from '@/app/utils/local-storage.service';
import marvelCharactersCartClient from '@/app/api-client/marvel-characters-cart-client';
import { MARVEL_CHARACTERS_CART_KEY } from '../marvel-characters/marvel-characters.constants';

class MarvelCharactersCartService {
  private marvelCharactersCartClient = marvelCharactersCartClient;

  private localStorageService = localStorageService;

  /**
   * Fetch all characters in user's cart
   */
  getMarvelCharactersCartByUserId(userId: number) {
    return this.marvelCharactersCartClient.get<any>(`users/${userId}/marvel-characters-cart`);
  }

  /**
   * Fetch all characters in user's cart from localStorage
   */
  getMarvelCharactersCartFromLocalStorage() {
    let marvelCharactersCart = this.localStorageService.get(MARVEL_CHARACTERS_CART_KEY);
    if (!marvelCharactersCart) {
      marvelCharactersCart = [];
    }
    return marvelCharactersCart;
  }

  /**
   * Udpate quantity character in user's cart
  */
  updateCharacterQuantityInCart(marvelCharacterCartId: number, quantity: number) {
    return this.marvelCharactersCartClient.put<AxiosResponse>(
      `marvel-characters-cart/${marvelCharacterCartId}/?quantity=${quantity}`,
    );
  }

  /**
   * Udpate quantity character in user's cart from localStorage
  */
  updateCharacterQuantityInCartFromLocalStorage(
    marvelCharacterCarName: string, quantity: number,
  ) {
    const marvelCharactersCart = this.localStorageService.get(MARVEL_CHARACTERS_CART_KEY);
    const findIndexUserByName = marvelCharactersCart.findIndex(
      (character: any) => character.marvelCharacterName === marvelCharacterCarName,
    );

    marvelCharactersCart[findIndexUserByName] = {
      ...marvelCharactersCart[findIndexUserByName],
      quantity: Number(quantity),
    };
    localStorageService.set(MARVEL_CHARACTERS_CART_KEY, marvelCharactersCart);
  }

  /**
   * Delete character in user's cart
  */
  deleteCharacterInCart(marvelCharacterCartId: number) {
    return this.marvelCharactersCartClient.delete<AxiosResponse>(
      `marvel-characters-cart/${marvelCharacterCartId}`,
    );
  }

  /**
   * Delete character in user's cart in localStorage
  */
  deleteCharacterInCartToLocalStorage(marvelCharacterCarName: string) {
    const marvelCharactersCart = this.localStorageService.get(MARVEL_CHARACTERS_CART_KEY);
    const newMarvelCharacters = marvelCharactersCart.filter(
      (character: any) => character.marvelCharacterName !== marvelCharacterCarName,
    );
    localStorageService.set(MARVEL_CHARACTERS_CART_KEY, newMarvelCharacters);
    return newMarvelCharacters;
  }
}

// Export a singleton instance in the global namespace
export default new MarvelCharactersCartService();
