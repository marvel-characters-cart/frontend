
class MarvelCharacterCartDto {
  id: number;

  quantity: number;

  marvelCharacterId: number;

  marvelCharacterName: string;

  marvelCharacterDescription: string;

  marvelCharacterImageUrl: string;
}

export default MarvelCharacterCartDto;
