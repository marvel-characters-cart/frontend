import localStorageService from '@/app/utils/local-storage.service';
import USER_KEY from './users.constants';

interface UserI {
  userId: number;

  username: string;
}

class UsersService {
  private localStorageService = localStorageService;

  /**
   * Check if user is Login
   */
  userIsLogin(): boolean {
    const userFromLocalStorage = this.localStorageService.get('user');
    return userFromLocalStorage;
  }

  getUserFromLocalStorage(): UserI {
    return this.localStorageService.get(USER_KEY);
  }
}

// Export a singleton instance in the global namespace
export default new UsersService();
