class AddCharacterToCartDto {
  readonly marvelCharacterId: number;

  readonly marvelCharacterName: string;

  readonly marvelCharacterDescription: string;

  readonly marvelCharacterImageUrl: string;
}

export default AddCharacterToCartDto;
