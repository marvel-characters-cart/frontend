import DESCRIPTION_NOT_FOUND from '../components/marvel-character-item/marvel-character-item.constants';

interface Image {
  /** The directory path of to the image. */
  readonly path: string;

  /** The file extension for the image. */
  readonly extension: string;
}

export default class MarvelCharacterDto {
  /** The unique ID of the character resource. */
  readonly id: number;

  /** The name of the character. */
  readonly name: string;

  /** A short bio or description of the character. */
  readonly description: string;

  /** A short bio or description of the character. */
  readonly thumbnail: Image;

  /**
   * Returns the image url of character compose by path + extension
   *
   * @returns image url
   *
   */
  get imageUrl(): string {
    return `${this.thumbnail.path}.${this.thumbnail.extension}`;
  }

  /**
  * Returns the Description of character depends if have description or not
  *
  * @returns the description or 'Description not found' if the description not found.
  */
  get characterDescription(): string {
    return this.description
      ? this.description
      : DESCRIPTION_NOT_FOUND;
  }
}
