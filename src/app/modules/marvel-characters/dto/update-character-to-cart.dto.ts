class UpdateCharacterToCartDto {
  readonly marvelCharacterId: number;

  readonly marvelCharacterName: string;

  readonly marvelCharacterDescription: string;

  readonly marvelCharacterImageUrl: string;

  readonly quantity: number;
}

export default UpdateCharacterToCartDto;
