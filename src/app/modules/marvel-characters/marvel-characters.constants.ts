export const DESCRIPTION_NOT_FOUND = 'Description not found';

export const MARVEL_CHARACTERS_CART_KEY = 'marvelCharactersCart';
