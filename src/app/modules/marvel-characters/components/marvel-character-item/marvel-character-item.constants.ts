const DESCRIPTION_NOT_FOUND = 'Description not found';

export default DESCRIPTION_NOT_FOUND;
