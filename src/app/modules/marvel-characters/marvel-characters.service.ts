import marvelApiClient from '@/app/api-client/marvel-api-client';
import { AxiosResponse } from 'axios';
import marvelCharactersCartClient from '@/app/api-client/marvel-characters-cart-client';
import localStorageService from '@/app/utils/local-storage.service';
import AddCharacterToCartDto from './dto/add-character-to-cart.dto';
import { MARVEL_CHARACTERS_CART_KEY } from './marvel-characters.constants';

class MarvelCharactersService {
  private marvelApiClient = marvelApiClient;

  private marvelCharactersCartClient = marvelCharactersCartClient;

  private localStorageService = localStorageService;

  /** Fetch characters of marvel api */
  getMarvelCharacters(offset: number) {
    return this.marvelApiClient.get<AxiosResponse>(
      `/characters?offset=${offset}&limit=100`,
    );
  }

  /**
   * Fetch all characters in user's cart
   */
  getMarvelCharactersCartByUserId(userId: number) {
    return this.marvelCharactersCartClient.get<AxiosResponse>(`users/${userId}/marvel-characters-cart`);
  }

  /**
   * Add Character to user's cart
   */
  addCharacterToCart(userId: number, addCharacterToCartDto: AddCharacterToCartDto) {
    return this.marvelCharactersCartClient.post<AxiosResponse>(
      `users/${userId}/marvel-characters-cart`,
      addCharacterToCartDto,
    );
  }

  /**
   * Add Character to user's cart to localStorage
   */
  addCharacterToCartToLocalStorage(addCharacterToCartDto: any) {
    const marvelCharactersIsCart = this.localStorageService.get(MARVEL_CHARACTERS_CART_KEY);
    if (!marvelCharactersIsCart) {
      const newMarvelCharacterCart = [
        { ...addCharacterToCartDto, quantity: 1 },
      ];
      this.localStorageService.set(MARVEL_CHARACTERS_CART_KEY, newMarvelCharacterCart);
    } else {
      const findIndexUserById = marvelCharactersIsCart.findIndex(
        (character: AddCharacterToCartDto) => character.marvelCharacterId
          === addCharacterToCartDto.marvelCharacterId,
      );

      if (findIndexUserById !== -1) {
        marvelCharactersIsCart[findIndexUserById] = {
          ...marvelCharactersIsCart[findIndexUserById],
          quantity: marvelCharactersIsCart[findIndexUserById].quantity + 1,
        };
        this.localStorageService.set(MARVEL_CHARACTERS_CART_KEY, marvelCharactersIsCart);
      } else {
        const newMarvelCharacterCart = { ...addCharacterToCartDto, quantity: 1 };
        marvelCharactersIsCart.push(newMarvelCharacterCart);
        this.localStorageService.set(MARVEL_CHARACTERS_CART_KEY, marvelCharactersIsCart);
      }
    }
  }
}

// Export a singleton instance in the global namespace
export default new MarvelCharactersService();
