export default [
  {
    path: '/marvel-characters',
    name: 'marvel-characters',
    component: () => import('./marvel-characters.vue'),
    title: 'marvel-characters',
  },
];
