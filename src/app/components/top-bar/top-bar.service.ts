import marvelCharactersCartClient from '@/app/api-client/marvel-characters-cart-client';
import UpdateCharacterToCartDto from '@/app/modules/marvel-characters/dto/update-character-to-cart.dto';

class TopBarService {
  private marvelCharactersCartClient = marvelCharactersCartClient;

  /**
   * Return user found
   */
  userLogin(username: string) {
    return this.marvelCharactersCartClient.get<any>(
      `users/login?name=${username}`,
    );
  }

  /**
   * Add all Characters in localStorage
   */
  async addCharactersToCart(
    userId: number,
    updateCharacterToCartDto: UpdateCharacterToCartDto[],
  ) {
    return this.marvelCharactersCartClient.post(
      `users/${userId}/add-marvel-characters-cart`,
      updateCharacterToCartDto,
    );
  }
}

export default new TopBarService();
