
class LocalStorageService {
  localStorage = localStorage;

  private stringify = (object: any) => {
    const objectToString = JSON.stringify(object);
    return objectToString;
  }

  private parse = (objecInString: string) => JSON.parse(objecInString);

  get(key: string): any {
    let item;
    if (localStorage.getItem(key)) {
      item = this.parse(localStorage.getItem(key) || '');
    }
    return item || null;
  }

  set(key: string, value: any) {
    const valueString = this.stringify(value);
    this.localStorage.setItem(key, valueString);
  }
}

export default new LocalStorageService();
