import Axios from 'axios';

const marvelCharactersCartClient = Axios.create({
  baseURL: 'http://localhost:3000/api',
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

export default marvelCharactersCartClient;
