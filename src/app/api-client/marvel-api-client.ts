import Axios, { AxiosRequestConfig } from 'axios';

const marvelApiClient = Axios.create({
  baseURL: 'http://gateway.marvel.com/v1/public',
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
});

marvelApiClient.interceptors.request.use((config: AxiosRequestConfig) => {
  const newConfig: AxiosRequestConfig = { ...config };
  // Add the necesary params required for marvel api
  newConfig.url = `${config.url}&ts=1&apikey=262036ee515f9f85dcca216a40ed0edc&hash=b5179f64a6016b2555ce0b6376e344e3`;
  return newConfig;
});

export default marvelApiClient;
