import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuetify from 'vuetify';
import MarvelCharacterItem from '@/app/modules/marvel-characters/components/marvel-character-item/marvel-character-item.vue';
import DESCRIPTION_NOT_FOUND from '@/app/modules/marvel-characters/components/marvel-character-item/marvel-character-item.constants';
import { MARVEL_CHARACTER_IMAGE_URL_TEST_CASE, MARVEL_CHARACTER_TEST_CASE } from './marvel-character-item-constants';


const localVue: any = createLocalVue();

const createMarvelCharacterItem = (propsData: any) => shallowMount(MarvelCharacterItem, {
  localVue,
  propsData,
});


describe('marvel-character-item.test.ts', () => {
  let marvelCharacterItem: any;

  beforeEach(() => {
    localVue.use(Vuetify);
    marvelCharacterItem = createMarvelCharacterItem(
      { marvelCharacter: MARVEL_CHARACTER_TEST_CASE },
    );
  });

  /* Test Properties */
  describe('Properties', () => {
    it('Has a marvelCharacter property', () => {
      expect(marvelCharacterItem.props().marvelCharacter)
        .toBe(MARVEL_CHARACTER_TEST_CASE);
    });
  });

  /* Test computed properties */
  describe('Computed properties', () => {
    describe('marvelCharacterImageUrl', () => {
      it('must be the combination of path + extension', () => {
        expect(marvelCharacterItem.vm.marvelCharacterImageUrl)
          .toBe(MARVEL_CHARACTER_IMAGE_URL_TEST_CASE);
      });
    });
    describe('marvelCharacterDescription', () => {
      it(`must return ${DESCRIPTION_NOT_FOUND} for empty description`, () => {
        const marvelCharacterTestCaseWithNoDescription = { ...MARVEL_CHARACTER_TEST_CASE, description: '' };
        marvelCharacterItem = createMarvelCharacterItem(
          { marvelCharacter: marvelCharacterTestCaseWithNoDescription },
        );
        expect(marvelCharacterItem.vm.marvelCharacterDescription)
          .toBe(DESCRIPTION_NOT_FOUND);
      });

      it('must return the not empty description', () => {
        expect(marvelCharacterItem.vm.marvelCharacterDescription)
          .toBe(MARVEL_CHARACTER_TEST_CASE.description);
      });
    });
  });

  /* Test events */
});
