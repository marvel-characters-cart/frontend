
import { plainToClass } from 'class-transformer';
import DESCRIPTION_NOT_FOUND from '@/app/modules/marvel-characters/components/marvel-character-item/marvel-character-item.constants';
import MarvelCharacterDto from '../../../../src/app/modules/marvel-characters/dto/marvel_character.dto';
import { MARVEL_CHARACTER_TEST_CASE, MARVEL_CHARACTER_IMAGE_URL_TEST_CASE } from '../marvel-character-item/marvel-character-item-constants';

const createMarvelCharacterDto = (plainObject: object) => plainToClass(
  MarvelCharacterDto, plainObject,
);

describe('marvel_character.dto.spec.ts', () => {
  let marvelCharacterDto: MarvelCharacterDto;

  beforeEach(() => {
    marvelCharacterDto = createMarvelCharacterDto(MARVEL_CHARACTER_TEST_CASE);
  });

  describe('getters', () => {
    describe('imageUrl', () => {
      it('must be the combination of path + extension', () => {
        expect(marvelCharacterDto.imageUrl)
          .toBe(MARVEL_CHARACTER_IMAGE_URL_TEST_CASE);
      });
    });
    describe('marvelCharacterDescription', () => {
      it(`must return ${DESCRIPTION_NOT_FOUND} for empty description`, () => {
        const marvelCharacterTestCaseWithNoDescription = createMarvelCharacterDto({
          ...MARVEL_CHARACTER_TEST_CASE, description: '',
        });
        expect(marvelCharacterTestCaseWithNoDescription.characterDescription)
          .toBe(DESCRIPTION_NOT_FOUND);
      });

      it('must return the not empty description', () => {
        expect(marvelCharacterDto.characterDescription)
          .toBe(MARVEL_CHARACTER_TEST_CASE.description);
      });
    });
  });
});
